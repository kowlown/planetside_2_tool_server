package com.mystnihon.planetside.scheduling;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.mystnihon.planetside.configuration.UrlConstants;
import com.mystnihon.planetside.database.entities.World;
import com.mystnihon.planetside.database.repositories.MetagameEventRepository;
import com.mystnihon.planetside.database.repositories.MetagameEventStateRepository;
import com.mystnihon.planetside.database.repositories.UpdateConfigRepository;
import com.mystnihon.planetside.database.repositories.WorldRepository;
import com.mystnihon.planetside.json.MetagameEventListContainer;
import com.mystnihon.planetside.json.MetagameEventStateListContainer;
import com.mystnihon.planetside.json.WorldListContainer;
import com.mystnihon.planetside.watcher.Watcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Default template
 * Created by llirvat on 23/11/2016.
 */
@Component
public class ConfigurationTask implements Runnable {

    private static final String UTF_8 = "utf-8";
    private static final String CONFIGURATION_RECEIVED = "Configuration received";
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationTask.class);

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    MetagameEventRepository metagameEventRepository;

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    MetagameEventStateRepository metagameEventStateRepository;

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    WorldRepository worldRepository;

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    UpdateConfigRepository updateConfigRepository;

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    Watcher watcher;

    @Override
    public void run() {
        try {
            LOGGER.debug("Starting configuration update");
            update();
        } catch (IOException e) {
            LOGGER.error("Exception while trying to get configuration", e);
        } finally {
            configure();
        }

    }

    private void update() throws IOException {
        getAlertConfiguration();
        getWorldConfiguration();
        getMetagameState();
    }

    private void getMetagameState() throws IOException {
        LOGGER.debug("Starting configuration update -> getMetagameState");
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        URL url = new URL(UrlConstants.URL_LIST_METAGAME_STATE);
        URLConnection connection = url.openConnection();
        connection.connect();

        final InputStream is = connection.getInputStream();
        LOGGER.debug(CONFIGURATION_RECEIVED);

        final BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName(UTF_8)));

        JsonReader jsonReader = new JsonReader(reader);
        MetagameEventStateListContainer metagameEventListContainer = gson.fromJson(jsonReader, MetagameEventStateListContainer.class);

        metagameEventStateRepository.deleteAll();
        metagameEventStateRepository.save(metagameEventListContainer.getMetagameEventState());

        is.close();
    }

    private void configure() {
        List<World> worlds = worldRepository.findAll();
        List<String> worldIds = worlds.stream().map(World::getWorldId).collect(Collectors.toList());

        String[] worldArray = new String[worldIds.size()];
        worldArray = worldIds.toArray(worldArray);
        watcher.configure(worldArray);

    }

    private void getWorldConfiguration() throws IOException {
        LOGGER.debug("Starting configuration update -> getWorldConfiguration");
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        URL url = new URL(UrlConstants.URL_LIST_WORLD);
        URLConnection connection = url.openConnection();
        connection.connect();

        final InputStream is = connection.getInputStream();
        LOGGER.debug(CONFIGURATION_RECEIVED);

        final BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName(UTF_8)));

        JsonReader jsonReader = new JsonReader(reader);
        WorldListContainer worldListContainer = gson.fromJson(jsonReader, WorldListContainer.class);

        worldRepository.deleteAll();
        worldRepository.save(worldListContainer.getWorldList());

        is.close();
    }

    private void getAlertConfiguration() throws IOException {
        LOGGER.debug("Starting configuration update -> getAlertConfiguration");
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        URL url = new URL(UrlConstants.URL_LIST_ALERT);
        URLConnection connection = url.openConnection();
        connection.connect();

        final InputStream is = connection.getInputStream();
        LOGGER.debug(CONFIGURATION_RECEIVED);

        final BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName(UTF_8)));

        JsonReader jsonReader = new JsonReader(reader);
        MetagameEventListContainer metagameEventListContainer = gson.fromJson(jsonReader, MetagameEventListContainer.class);

        metagameEventListContainer.getMetagameEvent().forEach(metagameEvent -> {
            metagameEvent.getDescription().setMetagameEventDesc(metagameEvent);
            metagameEvent.getName().setMetagameEvent(metagameEvent);
        });

        metagameEventRepository.deleteAll();
        metagameEventRepository.save(metagameEventListContainer.getMetagameEvent());

        is.close();

    }


}
