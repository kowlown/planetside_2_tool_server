package com.mystnihon.planetside.scheduling;

import com.mystnihon.planetside.database.repositories.MetagameEventStatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * Default template
 * Created by llirvat on 01/02/2017.
 */
@Component
public class RemoveOldMetagameStatsTask implements Runnable {

    @Autowired
    private MetagameEventStatsRepository metagameEventStatsRepository;

    @Override
    public void run() {
        Instant oneMonthBefore = Instant.now().minus(1, ChronoUnit.MONTHS);
        metagameEventStatsRepository.deleteInBatch(metagameEventStatsRepository.findByCreationTimeBefore(oneMonthBefore));

    }
}
