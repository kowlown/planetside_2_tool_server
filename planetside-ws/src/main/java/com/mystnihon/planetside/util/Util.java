package com.mystnihon.planetside.util;

import com.mystnihon.planetside.gcm.FileMissingException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Default template
 * Created by llirvat on 22/11/2016.
 */
public class Util {

    public static final String TEST_CONSTANT = "{\"payload\":{\"event_name\":\"MetagameEvent\",\"experience_bonus\":\"20.000000\",\"faction_nc\":\"0.000000\",\"faction_tr\":\"0.000000\",\"faction_vs\":\"0.000000\",\"metagame_event_id\":\"1\",\"metagame_event_state\":\"138\",\"timestamp\":\"1402952912\",\"world_id\":\"13\"},\"service\":\"event\",\"type\":\"serviceMessage\"}";

    public static String getResourcePropertyValue(Class<?> clazz, String propertyFilename, String keyName) throws FileMissingException, IOException {
        InputStream is = clazz.getClassLoader().getResourceAsStream(propertyFilename);
        Properties p = new Properties();
        if (is == null) {
            throw new FileMissingException();
        }

        p.load(is);
        is.close();
        return p.getProperty(keyName);
    }


}
