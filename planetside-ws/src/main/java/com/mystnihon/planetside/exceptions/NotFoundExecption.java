package com.mystnihon.planetside.exceptions;

/**
 * Default template
 * Created by llirvat on 19/12/2017.
 *
 * @author llirvat
 */
public class NotFoundExecption extends Exception {
    public NotFoundExecption(String s) {
        super(s);
    }
}
