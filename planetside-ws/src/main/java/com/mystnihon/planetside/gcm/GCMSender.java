package com.mystnihon.planetside.gcm;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.mystnihon.planetside.database.entities.AlertSubscription;
import com.mystnihon.planetside.database.entities.MetagameEvent;
import com.mystnihon.planetside.model.Alert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GCMSender {

    public enum NotificationType {
        ALERT("planetside_alert"), INFO("info"), OTHER("other");

        private String type;

        NotificationType(String type) {
            this.type = type;
        }

        public String getNotificationType() {
            return type;
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(GCMSender.class);
    private static final int MAX_RETRIES = 2;

    private Sender sender;

    public GCMSender(String key) throws FileMissingException, IOException {
        sender = new Sender(key);

    }

    public void sendAlert(Alert alert, MetagameEvent alertType, List<AlertSubscription> subscriptions) {

        String name = alertType.getName().toJSONObject().toString();
        String description = alertType.getDescription().toJSONObject().toString();

        //@formatter:off
        Message.Builder messageBuilder = new Message.Builder();
        messageBuilder
                .addData("eventState", String.valueOf(alert.getState().getText()))
                .addData("notificationType", NotificationType.ALERT.getNotificationType())
                .addData("eventType", String.valueOf(alert.getEventType()))
                .addData("expercienceBonus", String.valueOf(alert.getExperienceBonus()))
                .addData("expercienceBonusVS", String.valueOf(alert.getExperienceBonusVS()))
                .addData("expercienceBonusNC", String.valueOf(alert.getExperienceBonusNC()))
                .addData("expercienceBonusTR", String.valueOf(alert.getExperienceBonusTR()))
                .addData("worldId", String.valueOf(alert.getWorldId()))
                .addData("timestamp", String.valueOf(alert.getTimestamp()))
                .addData("name", name)
                .addData("description", description)

                .delayWhileIdle(true);
        //@formatter:on
        LOGGER.debug("Received an alert to send");
        LOGGER.debug(name);
        LOGGER.debug(description);

        ArrayList<String> list = new ArrayList<String>();
        for (AlertSubscription sub : subscriptions) {
            final String reg = sub.getRelatedSubscriber().getGcmKey();
            if (reg != null && reg.length() > 0)
                list.add(sub.getRelatedSubscriber().getGcmKey());
        }

        // Create the message and proceed to send it.
        Message message = messageBuilder.build();

        send(message, list);

    }

    private void send(Message message, ArrayList<String> list) {
        MulticastResult multicastResult;
        try {
            if (list.size() > 0) {
                multicastResult = sender.send(message, list, MAX_RETRIES);

                LOGGER.debug("Canonical registrationIds" + multicastResult.getCanonicalIds());

                for (Result result : multicastResult.getResults()) {

                    if (result.getErrorCodeName() != null && result.getErrorCodeName().length() > 0) {
                        LOGGER.error("Error for :" + result.getCanonicalRegistrationId() + " => "
                                + result.getErrorCodeName());
                        LOGGER.error("Couldn't send the GCM message");

                    } else {
                        LOGGER.debug("Sent the GCM message");
                    }

                }
            }

        } catch (IOException e) {
            LOGGER.error("Couldn't send the GCM message");
        }

    }

}
