package com.mystnihon.planetside.database.entities;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name = AlertSubscription.TABLE_NAME)
public class AlertSubscription {

    public static final String TABLE_NAME = "alert_subscription";
    public static final String COLUMN_NAME_ALERT_SUBSCRIBER_ID = "alert_subscriber_id";
    public static final String COLUMN_NAME_WORLD_ID = "world_id";
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private AlertSubscriber alertSubscriber;

    @Column(name = COLUMN_NAME_WORLD_ID)
    @Expose
    private String worldId;

    public AlertSubscription() {

    }

    public AlertSubscription(String worldId, AlertSubscriber relaAlertSubscriber) {
        this.worldId = worldId;
        this.alertSubscriber = relaAlertSubscriber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AlertSubscriber getRelatedSubscriber() {
        return alertSubscriber;
    }

    public void setRelatedSubscriber(AlertSubscriber relatedSubscriber) {
        this.alertSubscriber = relatedSubscriber;
    }

    public String getWorldId() {
        return worldId;
    }

    public void setWorldId(String worldId) {
        this.worldId = worldId;
    }
}
