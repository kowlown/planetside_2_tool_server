package com.mystnihon.planetside.database.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.mystnihon.planetside.json.converter.LocaleConverter;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity
@Table(name = World.TABLE_NAME)
public class World {
    public static final String TABLE_NAME = "world";
    public static final String COLUMN_NAME_DESCRIPTION = "description";
    public static final String COLUMN_NAME_NAME = "name";
    public static final String COLUMN_NAME_WORLD_ID = "world_id";

    @Id
    @GeneratedValue
    private Long id;

    @SerializedName("world_id")
    @Expose
    @Column(unique = true,name = COLUMN_NAME_WORLD_ID)
    private String worldId;

    @Column(name = COLUMN_NAME_NAME)
    @Expose
    @JsonAdapter(LocaleConverter.class)
    private String name;

    @Column(name = COLUMN_NAME_DESCRIPTION)
    @Expose
    @JsonAdapter(LocaleConverter.class)
    private String description;

    /**
     * @return The worldId
     */
    public String getWorldId() {
        return worldId;
    }

    /**
     * @param worldId The world_id
     */
    public void setWorldId(String worldId) {
        this.worldId = worldId;
    }


    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
