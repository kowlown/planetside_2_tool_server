package com.mystnihon.planetside.database.entities;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Default template
 * Created by llirvat on 01/02/2017.
 */
@Entity
@Table(name = MetagameEventStats.TABLE_NAME)

public class MetagameEventStats {
    public static final String TABLE_NAME = "metagame_event_stats";
    private static final String METAGAME_EVENT_ID = "metagame_event_id";
    private static final String EVENT_TYPE = "event_type";
    private static final String CREATION_TIME = "creation_time";
    private static final String C_STATE = "state";
    private static final String WORLD_ID = "world_id";

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = METAGAME_EVENT_ID)
    private int metagameEventId;
    @Column(name = EVENT_TYPE)
    private String eventType;
    @Column(name = CREATION_TIME)
    private Timestamp creationTime;
    @Column(name = C_STATE)
    private int state;
    @Column(name = WORLD_ID)
    private String worldId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMetagameEventId() {
        return metagameEventId;
    }

    public void setMetagameEventId(int metagameEventId) {
        this.metagameEventId = metagameEventId;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Timestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public void setWorldId(String worldId) {
        this.worldId = worldId;
    }

    public String getWorldId() {
        return worldId;
    }
}
