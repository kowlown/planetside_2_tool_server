package com.mystnihon.planetside.database.repositories;

import com.mystnihon.planetside.database.entities.AlertSubscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface AlertSubscriptionRepository extends JpaRepository<AlertSubscription, Long> {

    List<AlertSubscription> findByWorldId(String worldId);

}
