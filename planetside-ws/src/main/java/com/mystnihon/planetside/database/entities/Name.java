
package com.mystnihon.planetside.database.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Generated;
import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Generated("org.jsonschema2pojo")
@Entity
@Table (name = Name.TABLE_NAME)
public class Name {

    public static final String TABLE_NAME = "name";
    public static final String COLUMN_NAME_DE ="de";
    public static final String COLUMN_NAME_EN ="en";
    public static final String COLUMN_NAME_ES ="es";
    public static final String COLUMN_NAME_FR ="fr";
    public static final String COLUMN_NAME_IT ="it";
    public static final String COLUMN_NAME_METAGAME_EVENT_ID = "metagame_event_id";

    @Id
    @GeneratedValue
    protected long id;

    @Column(name = COLUMN_NAME_DE)
    @SerializedName("de")
    @Expose
    private String de;

    @Column(name = COLUMN_NAME_EN)
    @SerializedName("en")
    @Expose
    private String en;

    @Column(name = COLUMN_NAME_ES)
    @SerializedName("es")
    @Expose
    private String es;

    @Column(name = COLUMN_NAME_FR)
    @SerializedName("fr")
    @Expose
    private String fr;

    @Column(name = COLUMN_NAME_IT)
    @SerializedName("it")
    @Expose
    private String it;

    @OneToOne
    protected MetagameEvent metagameEvent;

    /**
     * @return The de
     */
    public String getDe() {
        return de;
    }

    /**
     * @param de The de
     */
    public void setDe(String de) {
        this.de = de;
    }

    /**
     * @return The en
     */
    public String getEn() {
        return en;
    }

    /**
     * @param en The en
     */
    public void setEn(String en) {
        this.en = en;
    }

    /**
     * @return The es
     */
    public String getEs() {
        return es;
    }

    /**
     * @param es The es
     */
    public void setEs(String es) {
        this.es = es;
    }

    /**
     * @return The fr
     */
    public String getFr() {
        return fr;
    }

    /**
     * @param fr The fr
     */
    public void setFr(String fr) {
        this.fr = fr;
    }

    /**
     * @return The it
     */
    public String getIt() {
        return it;
    }

    /**
     * @param it The it
     */
    public void setIt(String it) {
        this.it = it;
    }


    /* ****************************
     * Hibernate Methods
     *****************************/

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MetagameEvent getMetagameEvent() {
        return metagameEvent;
    }

    public void setMetagameEvent(MetagameEvent metagameEvent) {
        this.metagameEvent = metagameEvent;
    }

    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("de", URLEncoder.encode(de, "utf-8"));
            jsonObject.put("en", URLEncoder.encode(en, "utf-8"));
            jsonObject.put("es", URLEncoder.encode(es, "utf-8"));
            jsonObject.put("fr", URLEncoder.encode(fr, "utf-8"));
            jsonObject.put("it", URLEncoder.encode(it, "utf-8"));
        } catch (JSONException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }
}
