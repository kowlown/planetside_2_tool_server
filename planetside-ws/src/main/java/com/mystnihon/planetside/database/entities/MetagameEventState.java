
package com.mystnihon.planetside.database.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;

@Entity
@Table(name = MetagameEventState.TABLE_NAME)
public class MetagameEventState {

    public static final String TABLE_NAME = "metagame_event_state";
    private static final String COLUMN_METAGAME_EVENT_STATE_ID = "metagame_event_state_id";
    private static final String COLUMN_NAME = "name";

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = COLUMN_METAGAME_EVENT_STATE_ID)
    @SerializedName("metagame_event_state_id")
    @Expose
    private String metagameEventStateId;

    @Column(name = COLUMN_NAME)
    @SerializedName("name")
    @Expose
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMetagameEventStateId() {
        return metagameEventStateId;
    }

    public void setMetagameEventStateId(String metagameEventStateId) {
        this.metagameEventStateId = metagameEventStateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
