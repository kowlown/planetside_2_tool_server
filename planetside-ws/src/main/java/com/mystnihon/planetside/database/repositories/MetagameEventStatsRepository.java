package com.mystnihon.planetside.database.repositories;

import com.mystnihon.planetside.database.entities.MetagameEventStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;

/**
 * Default template
 * Created by llirvat on 01/02/2017.
 */
public interface MetagameEventStatsRepository extends JpaRepository<MetagameEventStats, Long> {

    List<MetagameEventStats> findByCreationTimeBefore(Instant instant);

    @Query(value = "SELECT DATE(creation_time) AS cDay, COUNT(*) AS cCount FROM " + MetagameEventStats.TABLE_NAME + " GROUP BY cDay", nativeQuery = true)
    List<Object[]> groupRequestBy();


    @Query(value = "SELECT m FROM MetagameEventStats as m WHERE m.creationTime IN \n (SELECT MAX(mes.creationTime) FROM MetagameEventStats as mes GROUP BY mes.metagameEventId, world_id) AND (m.state = 135 OR m.state = 136 OR m.state = 139)")
    List<MetagameEventStats> getCurrentAlerts();
}
