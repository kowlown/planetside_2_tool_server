package com.mystnihon.planetside.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mystnihon.planetside.database.entities.AlertSubscriber;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface AlertSubscriberRepository extends JpaRepository<AlertSubscriber, Long> {
	AlertSubscriber findByGcmKey(String gcmKey);

}
