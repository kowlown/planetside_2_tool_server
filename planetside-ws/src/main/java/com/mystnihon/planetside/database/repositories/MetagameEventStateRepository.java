package com.mystnihon.planetside.database.repositories;

import com.mystnihon.planetside.database.entities.MetagameEventState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default template
 * Created by llirvat on 01/02/2017.
 */
@Transactional
public interface MetagameEventStateRepository extends JpaRepository<MetagameEventState, Long> {


}
