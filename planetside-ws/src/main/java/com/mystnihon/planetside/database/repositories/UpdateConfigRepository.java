package com.mystnihon.planetside.database.repositories;

import com.mystnihon.planetside.database.entities.UpdateConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UpdateConfigRepository extends JpaRepository<UpdateConfig, Long> {

    UpdateConfig findByType(String type);
}
