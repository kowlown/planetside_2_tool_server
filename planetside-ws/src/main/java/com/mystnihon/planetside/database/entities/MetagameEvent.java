
package com.mystnihon.planetside.database.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import javax.persistence.*;

@Entity
@Table(name = MetagameEvent.TABLE_NAME)
@Generated("org.jsonschema2pojo")
public class MetagameEvent {

    public static final String TABLE_NAME = "metagame_event";
    public static final String COLUMN_NAME_ID = "id";
    public static final String COLUMN_NAME_METAGAME_EVENT_ID = "metagame_event_id";
    public static final String COLUMN_NAME_TYPE = "type";
    public static final String COLUMN_NAME_EXPERIENCE_BONUS = "experience_bonus";


    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, name = COLUMN_NAME_METAGAME_EVENT_ID)
    @SerializedName("metagame_event_id")
    @Expose
    private int metagameEventId;

    @OneToOne(mappedBy = "metagameEvent", cascade = CascadeType.ALL)
    @SerializedName("name")
    @Expose
    private Name name;

    @OneToOne(mappedBy = "metagameEventDesc", cascade = CascadeType.ALL)
    @SerializedName("description")
    @Expose
    private Description description;

    @Column(name = COLUMN_NAME_TYPE)
    @SerializedName("type")
    @Expose
    private String type;

    @Column(name = COLUMN_NAME_EXPERIENCE_BONUS)
    @SerializedName("experience_bonus")
    @Expose
    private String experienceBonus;

    /**
     * @return The metagameEventId
     */
    public int getMetagameEventId() {
        return metagameEventId;
    }

    /**
     * @param metagameEventId The metagame_event_id
     */
    public void setMetagameEventId(int metagameEventId) {
        this.metagameEventId = metagameEventId;
    }

    /**
     * @return The name
     */
    public Name getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(Name name) {
        this.name = name;
    }

    /**
     * @return The description
     */
    public Description getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(Description description) {
        this.description = description;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The experienceBonus
     */
    public String getExperienceBonus() {
        return experienceBonus;
    }

    /**
     * @param experienceBonus The experience_bonus
     */
    public void setExperienceBonus(String experienceBonus) {
        this.experienceBonus = experienceBonus;
    }

}
