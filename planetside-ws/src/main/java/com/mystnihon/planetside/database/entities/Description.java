
package com.mystnihon.planetside.database.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Generated;
import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Entity
@Table(name = Description.TABLE_NAME)
@Generated("org.jsonschema2pojo")
public class Description {

    public static final String TABLE_NAME = "description";
    public static final String COLUMN_NAME_METAGAME_EVENT_DESC_ID = "metagame_event_desc_id";
    public static final String COLUMN_NAME_DE ="de";
    public static final String COLUMN_NAME_EN ="en";
    public static final String COLUMN_NAME_ES ="es";
    public static final String COLUMN_NAME_FR ="fr";
    public static final String COLUMN_NAME_IT ="it";

    @Id
    @GeneratedValue
    protected Long id;

    @Column(name = COLUMN_NAME_DE)
    @SerializedName("de")
    @Expose
    private String de;

    @Column(name = COLUMN_NAME_EN)
    @SerializedName("en")
    @Expose
    private String en;

    @Column(name = COLUMN_NAME_ES)
    @SerializedName("es")
    @Expose
    private String es;

    @Column(name = COLUMN_NAME_FR)
    @SerializedName("fr")
    @Expose
    private String fr;

    @Column(name = COLUMN_NAME_IT)
    @SerializedName("it")
    @Expose
    private String it;

    @OneToOne
    protected MetagameEvent metagameEventDesc;


    /**
     * @return The de
     */
    public String getDe() {
        return de;
    }

    /**
     * @param de The de
     */
    public void setDe(String de) {
        this.de = de;
    }

    /**
     * @return The en
     */
    public String getEn() {
        return en;
    }

    /**
     * @param en The en
     */
    public void setEn(String en) {
        this.en = en;
    }

    /**
     * @return The es
     */
    public String getEs() {
        return es;
    }

    /**
     * @param es The es
     */
    public void setEs(String es) {
        this.es = es;
    }

    /**
     * @return The fr
     */
    public String getFr() {
        return fr;
    }

    /**
     * @param fr The fr
     */
    public void setFr(String fr) {
        this.fr = fr;
    }

    /**
     * @return The it
     */
    public String getIt() {
        return it;
    }

    /**
     * @param it The it
     */
    public void setIt(String it) {
        this.it = it;
    }

    /* ****************************
     * Hibernate Methods
     *****************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MetagameEvent getMetagameEventDesc() {
        return metagameEventDesc;
    }

    public void setMetagameEventDesc(MetagameEvent metagameEventDesc) {
        this.metagameEventDesc = metagameEventDesc;
    }

    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("de", URLEncoder.encode(de, "utf-8"));
            jsonObject.put("en", URLEncoder.encode(en, "utf-8"));
            jsonObject.put("es", URLEncoder.encode(es, "utf-8"));
            jsonObject.put("fr", URLEncoder.encode(fr, "utf-8"));
            jsonObject.put("it", URLEncoder.encode(it, "utf-8"));
        } catch (JSONException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }
}
