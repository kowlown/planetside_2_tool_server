package com.mystnihon.planetside.database.repositories;

import com.mystnihon.planetside.database.entities.MetagameEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
public interface MetagameEventRepository extends JpaRepository<MetagameEvent, Long> {

    Optional<MetagameEvent> findByMetagameEventId(int eventType);

}
