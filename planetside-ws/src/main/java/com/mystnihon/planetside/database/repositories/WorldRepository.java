package com.mystnihon.planetside.database.repositories;

import com.mystnihon.planetside.database.entities.World;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
public interface WorldRepository extends JpaRepository<World, Integer> {

    Optional<World> findByWorldId(String worldId);
}
