package com.mystnihon.planetside.database.entities;

import javax.persistence.*;

@Entity
@Table(name = UpdateConfig.TABLE_NAME)
public class UpdateConfig {

    public static final String TABLE_NAME = "update_config";
    public static final String COLUMN_NAME_UPDATE_TIME = "update_time";
    public static final String COLUMN_NAME_TYPE = "type";
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = COLUMN_NAME_TYPE)
    private String type;

    @Column(name = COLUMN_NAME_UPDATE_TIME)
    private Long updateTime;


    public UpdateConfig() {
        updateTime = System.currentTimeMillis();
    }

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }


}
