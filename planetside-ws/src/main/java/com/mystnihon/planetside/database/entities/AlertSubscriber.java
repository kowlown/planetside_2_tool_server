package com.mystnihon.planetside.database.entities;

import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = AlertSubscriber.TABLE_NAME)
public class AlertSubscriber {

	public static final String TABLE_NAME = "alert_subscriber";
	public static final String COLUMN_NAME_GCM_KEY = "gcm_key";
	@Id
	@GeneratedValue
	private Long id;

	@Expose
	@Column(unique = true,name = COLUMN_NAME_GCM_KEY)
	private String gcmKey;

	@Expose
	@OneToMany(mappedBy = "alertSubscriber", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	private List<AlertSubscription> subscriptions = new ArrayList<AlertSubscription>();

	@Expose
	@Transient
	private String secureToken;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGcmKey() {
		return gcmKey;
	}

	public void setGcmKey(String gcmKey) {
		this.gcmKey = gcmKey;
	}

	public List<AlertSubscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<AlertSubscription> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public String getSecureToken() {
		return secureToken;
	}

	public void setSecureToken(String secureToken) {
		this.secureToken = secureToken;
	}

}
