package com.mystnihon.planetside.watcher;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mystnihon.planetside.configuration.UrlConstants;
import com.mystnihon.planetside.json.wss.WSSMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;
import javax.websocket.CloseReason.CloseCodes;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("SpellCheckingInspection")
public class AlertWebSocketClient implements Runnable {

    public static final String WSS_ACTION_CLEAR_SUBSCRIBE = "clearSubscribe";
    public static final String WSS_SERVICE_EVENT = "event";
    public static final String WSS_ACTION_SUBSCRIBE = "subscribe";
    public static final String METAGAME_EVENT = "MetagameEvent";
    private static final String KEY_USER_AGENT = "User-Agent";
    private static final String KEY_ORIGIN = "Origin";
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";

    //    private static final String ORIGIN = "http://com.mystnihon";
    private static Logger LOGGER = LoggerFactory.getLogger(AlertWebSocketClient.class);

    private final Object waitLock = new Object();

    private Status mStatus = Status.IDLE;
    private IOnMessageListener mIOnMessageListener = null;
    private IOnStatusListener mIOnStatusListener;
    private volatile boolean mustReconnect = false;
    private String[] worldArrayIds;
    private Gson gson;

    private Endpoint endpoint = new Endpoint() {

        @Override
        public void onOpen(Session session, EndpointConfig config) {

            try {
                if (worldArrayIds != null) {

                    WSSMessage message = new WSSMessage();
                    message.setService(WSS_SERVICE_EVENT);
                    message.setAction(WSS_ACTION_SUBSCRIBE);
                    message.setWorlds(worldArrayIds);
                    message.setEventNames(new String[]{METAGAME_EVENT});
                    sendMessage(session, message);
                }
            } catch (IOException e) {
                LOGGER.error("IOException", e);
            }
            //noinspection Convert2Lambda Ne pas convertir en lambda car on a besoin d'instancier un objet à fournir au addMessageHandler
            session.addMessageHandler(new MessageHandler.Whole<String>() {

                public void onMessage(String message) {
                    LOGGER.debug("Received msg: " + message);
                    try {
                        if (mIOnMessageListener != null) {
                            mIOnMessageListener.onMessageReceived(new JSONObject(message));
                        }
                    } catch (JSONException e) {
                        LOGGER.error("JSONException while trying to convert text message from server to JSON", e);
                    }
                }
            });
        }

        @Override
        public void onError(Session session, Throwable throwable) {
            LOGGER.debug("onError");
            LOGGER.error("Connection error : logged because passed in method onError", throwable);
            super.onError(session, throwable);
        }

        @Override
        public void onClose(Session session, CloseReason closeReason) {
            LOGGER.debug("onClose");
            if (closeReason.getCloseCode() == CloseCodes.NORMAL_CLOSURE) {
                LOGGER.info("Connection closed normally");
            } else {
                LOGGER.warn("Connection not closed normally");
                LOGGER.warn("Will try to reconnect");
                mustReconnect = true;

                synchronized (waitLock) {
                    waitLock.notify();
                }
            }
            super.onClose(session, closeReason);
        }
    };
    private String mOrigin;

    public AlertWebSocketClient() {
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    public Status getStatus() {
        return mStatus;
    }

    public void stopWatching() {
        if (mStatus == Status.RUNNING) {
            publishStatus(Status.CLOSING);
        }
        mustReconnect = false;
        synchronized (waitLock) {
            waitLock.notify();
        }
    }

    public void setOnMessageListener(IOnMessageListener listener) {
        mIOnMessageListener = listener;
    }

    public void setOnStatusListener(IOnStatusListener listener) {
        mIOnStatusListener = listener;
    }

    public void run() {
        LOGGER.info("Starting WebSocketClient");
        Session session = null;

        ClientEndpointConfig.Configurator clientEndpointConfigurator = new ClientEndpointConfig.Configurator();

        Map<String, List<String>> headers = new HashMap<>();

        List<String> stringsOrigin = new ArrayList<>();
        stringsOrigin.add(mOrigin);
        headers.put(KEY_ORIGIN, stringsOrigin);
        List<String> stringsUserAgent = new ArrayList<>();
        stringsUserAgent.add(USER_AGENT);
        headers.put(KEY_USER_AGENT, stringsUserAgent);

        clientEndpointConfigurator.beforeRequest(headers);

        ClientEndpointConfig.Builder builder = ClientEndpointConfig.Builder.create();
        builder.configurator(clientEndpointConfigurator);
        ClientEndpointConfig config = builder.build();

        try {

            do {
                WebSocketContainer container = ContainerProvider.getWebSocketContainer();
                session = connect(container, config);
                publishStatus(Status.RUNNING);
                wait4TerminateSignal();
                if (mustReconnect) {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        LOGGER.warn("InterruptedException while waiting to reconnect", e);
                    }
                }
            } while (mustReconnect);

            unSubscribe(session);

        } catch (IOException | DeploymentException e) {
            LOGGER.error("Exception was thrown", e);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (IOException e) {
                    LOGGER.error("Exception was thrown", e);
                }
            }
        }
        publishStatus(Status.IDLE);
    }

    private void unSubscribe(Session session) throws IOException {
        WSSMessage message = new WSSMessage();
        message.setService(WSS_SERVICE_EVENT);
        message.setAction(WSS_ACTION_CLEAR_SUBSCRIBE);
        message.setAll("true");
        sendMessage(session, message);
    }

    private void wait4TerminateSignal() {
        synchronized (waitLock) {
            try {
                waitLock.wait();
            } catch (InterruptedException e) {
                LOGGER.warn("Interrupted wait on lock", e);
            }
        }
    }

    private void sendMessage(Session session, WSSMessage message) throws IOException {
        final String json = gson.toJson(message, WSSMessage.class);
        LOGGER.debug(json);
        session.getBasicRemote().sendText(json);
    }

    private Session connect(WebSocketContainer container, ClientEndpointConfig config) throws DeploymentException,
            IOException {

        URI uri = URI.create(UrlConstants.WSS_URL);

        LOGGER.info("Uri to connect to : " + uri.toString());
        Session session = container.connectToServer(endpoint, config, uri);
        mustReconnect = false;
        return session;
    }

    public void setWorldToListen(String... worldArray) {
        this.worldArrayIds = worldArray;
    }

    private void publishStatus(Status status) {
        if (mStatus != status && mIOnStatusListener != null) {
            mIOnStatusListener.onStatusChanged(status);
        }
        mStatus = status;
    }

    public void setOrigin(String origin) {
        mOrigin = origin;
    }

    public enum Status {
        RUNNING, CLOSING, IDLE
    }

}
