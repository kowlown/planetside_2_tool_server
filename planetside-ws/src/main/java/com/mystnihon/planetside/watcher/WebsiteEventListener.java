package com.mystnihon.planetside.watcher;

import com.mystnihon.planetside.scheduling.ConfigurationTask;
import com.mystnihon.planetside.scheduling.RemoveOldMetagameStatsTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
public class WebsiteEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebsiteEventListener.class);
    private boolean alreadySetup;

    @Autowired
    private Watcher watcher;

    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    private ConfigurationTask configurationTask;

    @Autowired
    private RemoveOldMetagameStatsTask removeOldMetagameStatsTask;

    @EventListener
    public void handleContextRefresh(@SuppressWarnings("UnusedParameters") ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }
        LOGGER.debug("context refreshed");
        taskExecutor.submitListenable(configurationTask).addCallback(new ListenableFutureCallback<Object>() {
            @Override
            public void onFailure(Throwable throwable) {
                LOGGER.debug("onFailure");
            }

            @Override
            public void onSuccess(Object o) {
                LOGGER.debug("onSuccess");
                watcher.launch();
            }
        });
        taskScheduler.schedule(configurationTask, new CronTrigger("0 0 0 * * *"));
        taskScheduler.schedule(removeOldMetagameStatsTask, new CronTrigger("0 0 0 * * *"));
        alreadySetup = true;
    }


    @EventListener
    public void handleContextClosed(@SuppressWarnings("UnusedParameters") ContextClosedEvent event) {
        LOGGER.debug("context closed");
        if (watcher != null && watcher.isRunning()) {
            LOGGER.debug("stopping");
            watcher.stop();
        }
    }

    @EventListener
    public void handleContextStopped(@SuppressWarnings("UnusedParameters") ContextStoppedEvent event) {
        LOGGER.debug("context stopped");
    }
}
