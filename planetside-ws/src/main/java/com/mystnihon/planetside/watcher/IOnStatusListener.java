package com.mystnihon.planetside.watcher;

/**
 * Default template
 * Created by Lévi LIRVAT on 13/06/2016.
 */
public interface IOnStatusListener {
    void onStatusChanged(AlertWebSocketClient.Status status);
}
