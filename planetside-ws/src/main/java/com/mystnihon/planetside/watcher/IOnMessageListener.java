package com.mystnihon.planetside.watcher;

import org.json.JSONObject;

public interface IOnMessageListener {
	void onMessageReceived(JSONObject object);
}
