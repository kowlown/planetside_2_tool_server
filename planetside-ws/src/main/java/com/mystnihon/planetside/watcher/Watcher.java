package com.mystnihon.planetside.watcher;

import com.google.common.collect.Lists;
import com.mystnihon.planetside.database.entities.AlertSubscription;
import com.mystnihon.planetside.database.entities.MetagameEvent;
import com.mystnihon.planetside.database.entities.MetagameEventStats;
import com.mystnihon.planetside.database.entities.World;
import com.mystnihon.planetside.database.repositories.AlertSubscriptionRepository;
import com.mystnihon.planetside.database.repositories.MetagameEventRepository;
import com.mystnihon.planetside.database.repositories.MetagameEventStatsRepository;
import com.mystnihon.planetside.database.repositories.WorldRepository;
import com.mystnihon.planetside.exceptions.NotFoundExecption;
import com.mystnihon.planetside.gcm.FileMissingException;
import com.mystnihon.planetside.gcm.GCMSender;
import com.mystnihon.planetside.model.Alert;
import com.mystnihon.planetside.model.AlertInfo;
import com.mystnihon.planetside.util.Util;
import com.pushraven.Notification;
import com.pushraven.Pushraven;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("singleton")
public class Watcher implements IOnMessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(Watcher.class);
    private static final String TOPICS_ALERT_WORLD = "/topics/alertWorld_";
    private static final String ALERT_WEBSOCKET_ORIGIN = "alert.websocket.origin";

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    private AlertSubscriptionRepository alertSubscriptionRepository;

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    private MetagameEventRepository metagameEventRepository;

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    private MetagameEventStatsRepository mMetagameEventStatsRepository;

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    private WorldRepository worldRepository;

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    private Environment mEnvironment;

    private AlertWebSocketClient alertWebSocketClient;
    private boolean running;

    private Map<String, AlertInfo> mActiveMetagameEvent;

    private Watcher() {
        super();
        mActiveMetagameEvent = new HashMap<>();
        alertWebSocketClient = new AlertWebSocketClient();
        alertWebSocketClient.setOnMessageListener(this);
        alertWebSocketClient.setOnStatusListener(status -> {
            LOGGER.debug("WebSocket status " + status);
            switch (status) {
                case CLOSING:
                case IDLE:
                    running = false;
                    break;
                case RUNNING:
                    running = true;
                    break;
                default:
                    break;
            }
        });
    }

    public void configure(String... worldIds) {
        alertWebSocketClient.setWorldToListen(worldIds);
    }

    public void launch() {
        alertWebSocketClient.setOrigin(mEnvironment.getProperty(ALERT_WEBSOCKET_ORIGIN));
        alertWebSocketClient.stopWatching();
        Thread thread = new Thread(alertWebSocketClient);
        thread.start();
    }

    public void stop() {
        alertWebSocketClient.stopWatching();
    }

    public void test() {
        LOGGER.debug("test");
        try {
            Pushraven.setKey(getFirebaseKey());

            Map<String, Object> data = new HashMap<>();
            data.put("eventState", "started");
            data.put("notificationType", GCMSender.NotificationType.ALERT.getNotificationType());
            data.put("eventType", "type");
            data.put("experienceBonus", "30");
            data.put("experienceBonusVS", "30");
            data.put("experienceBonusNC", "30");
            data.put("experienceBonusTR", "30");
            data.put("worldId", "15");
            data.put("timestamp", System.currentTimeMillis());

            Notification notification = new Notification();
            notification.to(TOPICS_ALERT_WORLD + "test");
            notification.data(data);
            Pushraven.push(notification);

        } catch (FileMissingException | IOException e) {
            LOGGER.error("Error in sendFirebaseAlert", e);
        }
    }

    public AlertWebSocketClient.Status getStatus() {
        return alertWebSocketClient.getStatus();
    }

    public List<AlertInfo> getActiveMetagameEventFromMemory() {
        return Lists.newArrayList(mActiveMetagameEvent.values());
    }

    public List<AlertInfo> getActiveMetagameEventFromDatabase() {
        return mMetagameEventStatsRepository.getCurrentAlerts().stream().map(metagameEventStats -> {
            AlertInfo alertInfo = new AlertInfo();
            alertInfo.setInstant(metagameEventStats.getCreationTime().toInstant());
            alertInfo.setMetagameEventId(metagameEventStats.getMetagameEventId());
            alertInfo.setWorldId(metagameEventStats.getWorldId());
            Optional<MetagameEvent> metagameEvent = metagameEventRepository.findByMetagameEventId(metagameEventStats.getMetagameEventId());
            if (metagameEvent.isPresent()) {
                alertInfo.setName(metagameEvent.get().getName());
                alertInfo.setDescription(metagameEvent.get().getDescription());
            }
            return alertInfo;
        }).collect(Collectors.toList());
    }


    @Override
    public void onMessageReceived(JSONObject object) {
        if (object.has(Alert.PAYLOAD)) {
            Alert alert = new Alert(object);
            try {
                searchForSubscriber(alert);
            } catch (NotFoundExecption notFoundExecption) {
                LOGGER.error("Could not found metagame event:", notFoundExecption);
            }
        }
    }

    private void searchForSubscriber(Alert alert) throws NotFoundExecption {
        MetagameEvent alertType = metagameEventRepository.findByMetagameEventId(alert.getEventType()).orElseThrow(() -> new NotFoundExecption("Alert type => " + alert.getEventType()));

        Instant instantOfAlert = Instant.now();


        String key = alert.getWorldId() + "_" + alert.getEventType();
        switch (alert.getState()) {
            case CANCELED:
            case ENDED:
            case ERROR:
                mActiveMetagameEvent.remove(key);
                break;
            case STARTED:
            case RESTARTED:
            case BONUSCHANGED:
                mActiveMetagameEvent.put(key, new AlertInfo(alert.getWorldId(), alert.getEventType(), alertType.getName(), alertType.getDescription(), instantOfAlert));
                break;
            default:
                LOGGER.error(String.format("Unknown event state : %d", alert.getEventState()));
                break;
        }

        List<AlertSubscription> results = alertSubscriptionRepository.findByWorldId(alert.getWorldId());

        sendGcmAlert(alert, alertType, results);
        sendFirebaseAlert(alert, alertType);

        MetagameEventStats metagameEventStats = new MetagameEventStats();
        metagameEventStats.setMetagameEventId(alertType.getMetagameEventId());
        metagameEventStats.setEventType(alertType.getType());
        metagameEventStats.setState(alert.getEventState());
        metagameEventStats.setCreationTime(Timestamp.from(instantOfAlert));
        metagameEventStats.setWorldId(alert.getWorldId());
        mMetagameEventStatsRepository.save(metagameEventStats);

    }

    private void sendFirebaseAlert(Alert alert, MetagameEvent alertType) {
        try {
            Pushraven.setKey(getFirebaseKey());

            Optional<World> oWorld = worldRepository.findByWorldId(alert.getWorldId());
            if (oWorld.isPresent()) {
                String name = alertType.getName().toJSONObject().toString();
                String description = alertType.getDescription().toJSONObject().toString();

                Map<String, Object> data = new HashMap<>();
                data.put("eventState", String.valueOf(alert.getState().getText()));
                data.put("notificationType", GCMSender.NotificationType.ALERT.getNotificationType());
                data.put("eventType", String.valueOf(alert.getEventType()));
                data.put("experienceBonus", String.valueOf(alert.getExperienceBonus()));
                data.put("experienceBonusVS", String.valueOf(alert.getExperienceBonusVS()));
                data.put("experienceBonusNC", String.valueOf(alert.getExperienceBonusNC()));
                data.put("experienceBonusTR", String.valueOf(alert.getExperienceBonusTR()));
                data.put("worldId", String.valueOf(alert.getWorldId()));
                data.put("timestamp", String.valueOf(alert.getTimestamp()));
                data.put("name", name);
                data.put("description", description);

                Notification notification = new Notification();
                notification.to(TOPICS_ALERT_WORLD + oWorld.get().getName().toLowerCase());
                notification.data(data);
                Pushraven.push(notification);
            } else {
                LOGGER.error("[PUSH] Could not retrieve world associated with the alert");
            }

        } catch (FileMissingException | IOException e) {
            LOGGER.error("Error in sendFirebaseAlert", e);
        }
    }


    private void sendGcmAlert(Alert alert, MetagameEvent alertType, List<AlertSubscription> results) {
        GCMSender gcmSender;
        try {
            gcmSender = new GCMSender(getGcmKey());
            gcmSender.sendAlert(alert, alertType, results);
        } catch (FileMissingException e) {
            LOGGER.error("Missing file key.properties", e);
        } catch (IOException e) {
            LOGGER.error("Exception while sending notification", e);
        }
    }

    private String getFirebaseKey() throws FileMissingException, IOException {
        return Util.getResourcePropertyValue(getClass(), "key.properties", "firebaseKey");
    }

    private String getGcmKey() throws FileMissingException, IOException {
        return Util.getResourcePropertyValue(getClass(), "key.properties", "gcmKey");
    }

    boolean isRunning() {
        return running;
    }
}
