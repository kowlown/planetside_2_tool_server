package com.mystnihon.planetside.mapping;

import com.mystnihon.planetside.database.entities.Description;
import com.mystnihon.planetside.database.entities.Name;
import com.mystnihon.planetside.model.AlertInfo;
import com.mystnihon.planetside.model.dto.AlertInfoDto;
import com.mystnihon.planetside.model.dto.DescriptionDto;
import com.mystnihon.planetside.model.dto.NameDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Default Java Header
 * Created by Levi on 20/12/2017.
 */
@Mapper(componentModel = "spring")
public interface AlertInfoMapper {

    AlertInfoDto businessToDto(AlertInfo alertInfo);

    List<AlertInfoDto> listToDto(List<AlertInfo> alertInfos);

    DescriptionDto businessToDto(Description description);

    NameDto businessToDto(Name name);
}
