package com.mystnihon.planetside.model;

import com.mystnihon.planetside.database.entities.Description;
import com.mystnihon.planetside.database.entities.Name;

import java.time.Instant;

/**
 * Default template
 * Created by llirvat on 20/12/2017.
 *
 * @author llirvat
 */
public class AlertInfo {
    private String worldId;
    private int metagameEventId;
    private Name name;
    private Description description;
    private Instant instant;

    public AlertInfo(String worldId, int metagameEventId, Name name, Description description, Instant instant) {
        this.worldId = worldId;
        this.metagameEventId = metagameEventId;
        this.name = name;
        this.description = description;
        this.instant = instant;
    }

    public AlertInfo() {

    }

    public String getWorldId() {
        return worldId;
    }

    public void setWorldId(String worldId) {
        this.worldId = worldId;
    }

    public int getMetagameEventId() {
        return metagameEventId;
    }

    public void setMetagameEventId(int metagameEventId) {
        this.metagameEventId = metagameEventId;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant instant) {
        this.instant = instant;
    }

    public Name getName() {
        return name;
    }

    public Description getDescription() {
        return description;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public void setDescription(Description description) {
        this.description = description;
    }
}
