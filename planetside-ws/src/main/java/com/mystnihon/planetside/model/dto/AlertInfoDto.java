package com.mystnihon.planetside.model.dto;

import java.time.Instant;

/**
 * Default template
 * Created by llirvat on 20/12/2017.
 *
 * @author llirvat
 */
public class AlertInfoDto {
    private String worldId;
    private int metagameEventId;
    private Instant instant;
    private NameDto name;
    private DescriptionDto description;

    public String getWorldId() {
        return worldId;
    }

    public void setWorldId(String worldId) {
        this.worldId = worldId;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant instant) {
        this.instant = instant;
    }

    public int getMetagameEventId() {
        return metagameEventId;
    }

    public void setMetagameEventId(int metagameEventId) {
        this.metagameEventId = metagameEventId;
    }

    public NameDto getName() {
        return name;
    }

    public void setName(NameDto name) {
        this.name = name;
    }

    public DescriptionDto getDescription() {
        return description;
    }

    public void setDescription(DescriptionDto description) {
        this.description = description;
    }
}
