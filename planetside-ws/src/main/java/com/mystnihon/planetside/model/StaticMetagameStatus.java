package com.mystnihon.planetside.model;

public enum StaticMetagameStatus {

	STARTED(135, "started"), RESTARTED(136, "restarted"), CANCELED(137,
			"canceled"), ENDED(138, "ended"), BONUSCHANGED(139, "bonuschanged"), ERROR(
			0, "error");

	private int statusCode;
	private String text;

	StaticMetagameStatus(int statusCode, String text) {
		this.statusCode = statusCode;
		this.text = text;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public String getText() {
		return text;
	}
}
