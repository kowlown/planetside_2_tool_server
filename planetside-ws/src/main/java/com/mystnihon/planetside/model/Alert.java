package com.mystnihon.planetside.model;

import org.json.JSONObject;

import java.util.HashMap;

public class Alert {
    //TODO Use https://census.daybreakgames.com/s:kowlown971/get/ps2/metagame_event_state?c:limit=100 for state

    private static HashMap<Integer, StaticMetagameStatus> lookup = buildStaticMetagameStatus();
    private static final String TIMESTAMP2 = "timestamp";
    private static final String WORLD_ID = "world_id";
    private static final String FACTION_TR = "faction_tr";
    private static final String FACTION_VS = "faction_vs";
    private static final String FACTION_NC = "faction_nc";
    private static final String EXPERIENCE_BONUS = "experience_bonus";
    private static final String METAGAME_EVENT_STATE = "metagame_event_state";
    private static final String METAGAME_EVENT_ID = "metagame_event_id";
    public static final String PAYLOAD = "payload";

    private int eventType;
    private int eventState;
    private double experienceBonus;
    private double experienceBonusNC;
    private double experienceBonusVS;
    private double experienceBonusTR;
    private String worldId;
    private int timestamp;

    public Alert(JSONObject object) {
        JSONObject payloadJSONObject = object.getJSONObject(PAYLOAD);
        eventType = payloadJSONObject.optInt(METAGAME_EVENT_ID);
        eventState = payloadJSONObject.optInt(METAGAME_EVENT_STATE);
        experienceBonus = payloadJSONObject.optDouble(EXPERIENCE_BONUS);
        experienceBonusNC = payloadJSONObject.optDouble(FACTION_NC);
        experienceBonusVS = payloadJSONObject.optDouble(FACTION_VS);
        experienceBonusTR = payloadJSONObject.optDouble(FACTION_TR);
        worldId = payloadJSONObject.optString(WORLD_ID);
        timestamp = payloadJSONObject.optInt(TIMESTAMP2);
    }

    private static HashMap<Integer, StaticMetagameStatus> buildStaticMetagameStatus() {
        HashMap<Integer, StaticMetagameStatus> b = new HashMap<>();
        for (StaticMetagameStatus stat : StaticMetagameStatus.values()) {
            b.put(stat.getStatusCode(), stat);
        }
        return b;
    }

    public StaticMetagameStatus getState() {
        return lookup.get(eventState);
    }

    public int getEventType() {
        return eventType;
    }

    public int getEventState() {
        return eventState;
    }

    public double getExperienceBonus() {
        return experienceBonus;
    }

    public double getExperienceBonusNC() {
        return experienceBonusNC;
    }

    public double getExperienceBonusVS() {
        return experienceBonusVS;
    }

    public double getExperienceBonusTR() {
        return experienceBonusTR;
    }

    public String getWorldId() {
        return worldId;
    }

    public int getTimestamp() {
        return timestamp;
    }

}
