package com.mystnihon.planetside.configuration;

public class UrlConstants {

    public static final String URL_LIST_ALERT = "https://census.daybreakgames.com/get/ps2/metagame_event?c:limit=100";
    public static final String URL_LIST_WORLD = "https://census.daybreakgames.com/get/ps2/world?c:limit=100";
    public static final String URL_LIST_METAGAME_STATE = "https://census.daybreakgames.com/s:kowlown971/get/ps2/metagame_event_state?c:limit=100";
    public static final String WSS_URL = "wss://push.planetside2.com/streaming?environment=ps2&service-id=s:kowlown971";

}
