package com.mystnihon.planetside.configuration.security;

import com.mystnihon.planetside.security.LoginAttemptService;
import com.mystnihon.planetside.security.MyInMemoryUserDetailsManager;
import com.mystnihon.planetside.security.MyInMemoryUserDetailsManagerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

@Configuration
@EnableWebSecurity
@PropertySources({
        @PropertySource("classpath:access.properties"),
        @PropertySource(value = "file:${appconfig}/access.properties", ignoreResourceNotFound = true)
})
public class SecurityConfig {

    public static final String ADMIN_ACCESS_LOGIN = "admin.access.login";
    public static final String ADMIN_ACCESS_PASSWORD = "admin.access.password";
    public static final String APPLICATION_ACCESS_LOGIN = "application.access.login";
    public static final String APPLICATION_ACCESS_PASSWORD = "application.access.password";
    public static final String ADMIN_ROLE = "ADMIN";
    public static final String APPLICATION_ROLE = "APPLICATION";

    @Autowired
    LoginAttemptService loginAttemptService;
    @Autowired
    Environment environment;
    @Autowired
    private HttpServletRequest request;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationEventPublisher(new AuthenticationEventPublisher() {

            @Override
            public void publishAuthenticationSuccess(Authentication auth) {
                final String remoteAdress = ((WebAuthenticationDetails) auth.getDetails()).getRemoteAddress();
                loginAttemptService.loginSucceeded(remoteAdress);
            }

            @Override
            public void publishAuthenticationFailure(AuthenticationException arg0, Authentication auth) {
                final String remoteAdress = ((WebAuthenticationDetails) auth.getDetails()).getRemoteAddress();
                loginAttemptService.loginFailed(remoteAdress);

            }
        });

        MyInMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> builder = new MyInMemoryUserDetailsManagerConfigurer<>(getUs());
        builder.withUser(environment.getProperty(ADMIN_ACCESS_LOGIN)).password(environment.getProperty(ADMIN_ACCESS_PASSWORD)).roles(ADMIN_ROLE);
        builder.withUser(environment.getProperty(APPLICATION_ACCESS_LOGIN)).password(environment.getProperty(APPLICATION_ACCESS_PASSWORD)).roles(APPLICATION_ROLE);
        builder.passwordEncoder(encoder());
        builder.configure(auth);

    }

    @Bean
    public UserDetailsManager getUs() {
        return new MyInMemoryUserDetailsManager();
    }


    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Configuration
    @Order(1)
    public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable()
                    .antMatcher("/rest/**").authorizeRequests().anyRequest().hasAnyRole("APPLICATION", "ADMIN").and().httpBasic();
        }
    }

    @Configuration
    public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers("/j_spring_security_check*", "/login*", "/logout*").permitAll()
                    .antMatchers("/invalidSession*").anonymous()
                    .antMatchers("/admin/**").hasRole("ADMIN").and().formLogin()
                    .and().httpBasic();

        }
    }

}