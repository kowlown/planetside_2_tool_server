package com.mystnihon.planetside.configuration;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;

/**
 * Default template
 * Created by llirvat on 20/12/2017.
 * <p>
 * Need "commons-io:commons-io:2.6"
 *
 * @author llirvat
 */
@Profile("DockerSecret")
@Configuration
@ConfigurationProperties(prefix = "docker.datasource")
public class DBConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBConfiguration.class);
    private String secret;


    @ConfigurationProperties(prefix = "spring.datasource")
    @Component
    public static class DefaultDataSourceProperty {
        private String url;

        private String username;

        private String password;

        private String driverClassName;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getDriverClassName() {
            return driverClassName;
        }

        public void setDriverClassName(String driverClassName) {
            this.driverClassName = driverClassName;
        }
    }

    @Bean
    @Primary
    public DataSource getDataSource(DefaultDataSourceProperty dataSourceProperty) {

        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.type(DriverManagerDataSource.class);

        String password = get().trim();
        if (!password.isEmpty()) {
            LOGGER.debug("Secret is not empty:" + password);
        }
        dataSourceBuilder.driverClassName(dataSourceProperty.driverClassName);
        dataSourceBuilder.url(dataSourceProperty.url);
        dataSourceBuilder.username(dataSourceProperty.username);
        dataSourceBuilder.password(password);
        return dataSourceBuilder.build();
    }

    private String get() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(IOUtils.toString(new FileReader(secret)));
            LOGGER.info("Getting secret data");
        } catch (IOException e) {
            LOGGER.error("File not found", e);
        }
        return sb.toString();
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
