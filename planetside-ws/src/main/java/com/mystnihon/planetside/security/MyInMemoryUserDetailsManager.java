package com.mystnihon.planetside.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class MyInMemoryUserDetailsManager extends InMemoryUserDetailsManager {

    private static final String BLOCKED = "Blocked";
    @Autowired
    private LoginAttemptService loginAttemptService;

    public MyInMemoryUserDetailsManager(Collection<UserDetails> users) {
        super(users);
    }

    public MyInMemoryUserDetailsManager() {
        super(new ArrayList<>());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (loginAttemptService.isBlocked(username)) {
            throw new RuntimeException(BLOCKED);
        }
        return super.loadUserByUsername(username);
    }
}
