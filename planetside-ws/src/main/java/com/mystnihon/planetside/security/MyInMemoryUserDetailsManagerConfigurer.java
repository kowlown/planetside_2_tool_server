package com.mystnihon.planetside.security;

import org.springframework.security.config.annotation.authentication.ProviderManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.UserDetailsManagerConfigurer;
import org.springframework.security.provisioning.UserDetailsManager;

public class MyInMemoryUserDetailsManagerConfigurer<B extends ProviderManagerBuilder<B>> extends
        UserDetailsManagerConfigurer<B, InMemoryUserDetailsManagerConfigurer<B>> {

    /**
     * Creates a new instance
     */
    public MyInMemoryUserDetailsManagerConfigurer(UserDetailsManager userDetailsManager) {
        super(userDetailsManager);
    }

}
