package com.mystnihon.planetside.security;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Checker {

	private final List<String> mClientIDs;
	private final String mAudience;
	private final GoogleIdTokenVerifier mVerifier;
	private final JsonFactory mJFactory;
	private String mProblem = "Verification failed. (Time-out?)";

	public Checker(String[] clientIDs, String audience) {
		mClientIDs = Arrays.asList(clientIDs);
		mAudience = audience;
		NetHttpTransport transport = new NetHttpTransport();
		mJFactory = new GsonFactory();
		mVerifier = new GoogleIdTokenVerifier.Builder(transport, mJFactory).setAudience(Collections.singletonList(audience)).setIssuer("https://accounts.google.com").build();

	}

	public GoogleIdToken.Payload check(String tokenString) {

		GoogleIdToken.Payload payload = null;
		try {
			GoogleIdToken token = mVerifier.verify(tokenString);
			if (mVerifier.verify(token)) {
				GoogleIdToken.Payload tempPayload = token.getPayload();
				if (!tempPayload.getAudience().equals(mAudience))
					mProblem = "Audience mismatch";
				else if (!mClientIDs.contains(tempPayload.getAuthorizedParty()))
					mProblem = "Client COLUMN_NAME_ID mismatch";
				else
					payload = tempPayload;
			}
		} catch (GeneralSecurityException e) {
			mProblem = "Security issue: " + e.getLocalizedMessage();
		} catch (IOException e) {
			mProblem = "Network problem: " + e.getLocalizedMessage();
		}
		return payload;
	}

	public String problem() {
		return mProblem;
	}
}