package com.mystnihon.planetside.json;

import com.google.gson.annotations.Expose;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Default template
 * Created by Lévi LIRVAT on 13/06/2016.
 */
public class WatcherResult extends Result {

    @Expose
    public String status;

    public static ResponseEntity<Result> success(String status) {
        WatcherResult result = new WatcherResult();
        result.success = true;
        result.status = status;
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
