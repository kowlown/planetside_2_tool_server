package com.mystnihon.planetside.json.wss;

import com.google.gson.annotations.Expose;

public class WSSMessage {
    
    @Expose
    private String service;
    
    @Expose
    private String action;
    
    @Expose
    private String all;
    
    @Expose
    private String[] worlds;

    @Expose
    private String[] eventNames;

    public String getService() {
        return service;
    }

    public String getAction() {
        return action;
    }

    public String getAll() {
        return all;
    }

    public String[] getWorlds() {
        return worlds;
    }

    public String[] getEventNames() {
        return eventNames;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setAll(String all) {
        this.all = all;
    }

    public void setWorlds(String[] worlds) {
        this.worlds = worlds;
    }

    public void setEventNames(String[] eventNames) {
        this.eventNames = eventNames;
    }
}
