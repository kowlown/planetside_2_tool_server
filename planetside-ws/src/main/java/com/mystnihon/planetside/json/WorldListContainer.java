package com.mystnihon.planetside.json;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mystnihon.planetside.database.entities.World;


public class WorldListContainer {

    @SerializedName("world_list")
    @Expose
    private List<World> worldList = new ArrayList<World>();
    @Expose
    private Long returned;

    /**
     * 
     * @return
     *     The worldList
     */
    public List<World> getWorldList() {
        return worldList;
    }

    /**
     * 
     * @param worldList
     *     The world_list
     */
    public void setWorldList(List<World> worldList) {
        this.worldList = worldList;
    }

    /**
     * 
     * @return
     *     The returned
     */
    public Long getReturned() {
        return returned;
    }

    /**
     * 
     * @param returned
     *     The returned
     */
    public void setReturned(Long returned) {
        this.returned = returned;
    }

}
