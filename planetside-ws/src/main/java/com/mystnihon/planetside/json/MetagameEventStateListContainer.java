
package com.mystnihon.planetside.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mystnihon.planetside.database.entities.MetagameEventState;

import java.util.List;

public class MetagameEventStateListContainer {

    @SerializedName("metagame_event_state_list")
    @Expose
    private List<MetagameEventState> metagameEventState = null;
    @SerializedName("returned")
    @Expose
    private Integer returned;

    public List<MetagameEventState> getMetagameEventState() {
        return metagameEventState;
    }

    public void setMetagameEventState(List<MetagameEventState> metagameEventState) {
        this.metagameEventState = metagameEventState;
    }

    public Integer getReturned() {
        return returned;
    }

    public void setReturned(Integer returned) {
        this.returned = returned;
    }

}
