package com.mystnihon.planetside.json;

import com.google.gson.annotations.Expose;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Result {

    @Expose
    public boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static ResponseEntity<Result> success() {
        Result result = new Result();
        result.success = true;
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public static ResponseEntity<Result> fail() {
        Result result = new Result();
        result.success = false;
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
