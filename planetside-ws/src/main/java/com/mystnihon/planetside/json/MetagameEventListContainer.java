
package com.mystnihon.planetside.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mystnihon.planetside.database.entities.MetagameEvent;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class MetagameEventListContainer {

    @SerializedName("metagame_event_list")
    @Expose
    private List<MetagameEvent> metagameEvent = new ArrayList<MetagameEvent>();
    @SerializedName("returned")
    @Expose
    private Integer returned;

    /**
     * 
     * @return
     *     The metagameEventList
     */
    public List<MetagameEvent> getMetagameEvent() {
        return metagameEvent;
    }

    /**
     * 
     * @param metagameEvent
     *     The metagame_event_list
     */
    public void setMetagameEvent(List<MetagameEvent> metagameEvent) {
        this.metagameEvent = metagameEvent;
    }

    /**
     * 
     * @return
     *     The returned
     */
    public Integer getReturned() {
        return returned;
    }

    /**
     * 
     * @param returned
     *     The returned
     */
    public void setReturned(Integer returned) {
        this.returned = returned;
    }

}
