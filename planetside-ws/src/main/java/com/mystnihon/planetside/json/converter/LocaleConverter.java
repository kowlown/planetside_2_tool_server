package com.mystnihon.planetside.json.converter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Locale;

import org.springframework.util.StringUtils;

/**
 * Default template Created by Lévi LIRVAT on 05/08/2015.
 */
public class LocaleConverter extends TypeAdapter<String> {

	@Override
	public void write(JsonWriter out, String value) throws IOException {

	}

	@Override
	public String read(JsonReader in) throws IOException {
		final String localeCode = Locale.getDefault().getLanguage();
		String valueLocale = "";
		in.beginObject();
		while (in.hasNext()) {
			String name = in.nextName();
			final boolean isNull = in.peek() == JsonToken.NULL;
			if (name.equals(localeCode) && !isNull) {
				valueLocale = in.nextString();
			} else if (name.equals(Locale.ENGLISH.getLanguage()) && StringUtils.isEmpty(valueLocale)) {
				valueLocale = in.nextString();
			} else {
				in.skipValue();
			}
		}
		in.endObject();
		return valueLocale;
	}
}
