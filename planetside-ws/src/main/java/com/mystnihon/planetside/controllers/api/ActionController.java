package com.mystnihon.planetside.controllers.api;

import com.mystnihon.planetside.json.AdminAction;
import com.mystnihon.planetside.json.Result;
import com.mystnihon.planetside.json.WatcherResult;
import com.mystnihon.planetside.watcher.AlertWebSocketClient;
import com.mystnihon.planetside.watcher.Watcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Default template
 * Created by Lévi LIRVAT on 13/06/2016.
 */

@RestController
@RequestMapping("/admin")
public class ActionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActionController.class);

    @Autowired
    private Watcher watcher;

    @RequestMapping(value = "/home/action", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Result> actionPage(@RequestParam String action) {
        LOGGER.debug("actionPage");
        switch (action) {
            case AdminAction.TEST:
                watcher.test();
                return WatcherResult.success(watcher.getStatus().name());
            case AdminAction.START:
                watcher.launch();
                return WatcherResult.success(watcher.getStatus().name());
            case AdminAction.STOP:
                watcher.stop();
                return WatcherResult.success(watcher.getStatus().name());
            default:
                return WatcherResult.fail();
        }


    }

    @RequestMapping(value = "/home/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AlertWebSocketClient.Status> actionGetStatus() {
        LOGGER.debug("actionGetStatus");
        return new ResponseEntity<>(watcher.getStatus(), HttpStatus.OK);
    }
}
