package com.mystnihon.planetside.controllers.api;

import com.mystnihon.planetside.mapping.AlertInfoMapper;
import com.mystnihon.planetside.model.dto.AlertInfoDto;
import com.mystnihon.planetside.watcher.Watcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Default Java Header
 * Created by Levi on 20/12/2017.
 */
@RestController
@RequestMapping("/rest")
public class MetagameInfoController {

    @Autowired
    private Watcher watcher;

    @Autowired
    private AlertInfoMapper mAlertInfoMapper;

    @RequestMapping(value = "/alerts_memory", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AlertInfoDto>> getAlertsFromMemory() {
        return new ResponseEntity<>(mAlertInfoMapper.listToDto(watcher.getActiveMetagameEventFromMemory()), HttpStatus.OK);
    }

    @RequestMapping(value = "/alerts_database", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AlertInfoDto>> getAlertsFromDatabase() {
        return new ResponseEntity<>(mAlertInfoMapper.listToDto(watcher.getActiveMetagameEventFromDatabase()), HttpStatus.OK);
    }
}
