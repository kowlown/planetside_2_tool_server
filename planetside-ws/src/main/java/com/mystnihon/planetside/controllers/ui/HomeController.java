package com.mystnihon.planetside.controllers.ui;

import com.mystnihon.planetside.database.repositories.AlertSubscriberRepository;
import com.mystnihon.planetside.database.repositories.MetagameEventStatsRepository;
import com.mystnihon.planetside.watcher.Watcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class HomeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private Watcher watcher;

    @Autowired
    private AlertSubscriberRepository alertSubscriberRepository;

    @Autowired
    private MetagameEventStatsRepository metagameEventStatsRepository;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView adminPage() {
        ModelAndView model = new ModelAndView("admin");
        model.addObject("watcherStatus", watcher.getStatus());
        model.addObject("numberRecord", alertSubscriberRepository.count());
        List<Object[]> objects = metagameEventStatsRepository.groupRequestBy();
        model.addObject("countAlertByDay", objects);

        return model;

    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
    }

}
