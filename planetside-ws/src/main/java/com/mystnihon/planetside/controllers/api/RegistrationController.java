package com.mystnihon.planetside.controllers.api;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.mystnihon.planetside.database.entities.AlertSubscriber;
import com.mystnihon.planetside.json.Result;
import com.mystnihon.planetside.database.repositories.AlertSubscriberRepository;
import com.mystnihon.planetside.security.Checker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/rest")
@RestController
public class RegistrationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

    private static final String AUDIENCE_ID = "824484079884-b2vq179s1ehi4j5k0mkfmmmogm7k0f7u.apps.googleusercontent.com";

    private static final String[] CLIENT_ID = {"824484079884-2rsv7qtm805o5a88j9spthls6ooke2ec.apps.googleusercontent.com", "824484079884-bt8mbnte0ado7d0uin6d5han77oi00cg.apps.googleusercontent.com", "824484079884-n6a4vssr49bcgo146s17t5c8cif6g0j9.apps.googleusercontent.com"};

    @Autowired
    private AlertSubscriberRepository alertSubscriberRepository;

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Result> register(@RequestBody AlertSubscriber subscriber) {
        LOGGER.debug("[REGISTER IN]");
        subscriber.getSubscriptions().forEach(subs -> subs.setRelatedSubscriber(subscriber));

        AlertSubscriber recordedSubscriber = alertSubscriberRepository.findByGcmKey(subscriber.getGcmKey());

        LOGGER.debug(String.format("Secure token -> %s", subscriber.getSecureToken()));

        Checker checker = new Checker(CLIENT_ID, AUDIENCE_ID);

        Payload payload = checker.check(subscriber.getSecureToken());
        if (payload != null) {
            // TODO Make an update instead of delete/save.
            if (recordedSubscriber != null) {
                alertSubscriberRepository.delete(recordedSubscriber);
            }
            alertSubscriberRepository.save(subscriber);
            LOGGER.debug("[REGISTER OUT]");
            return Result.success();
        } else {
            LOGGER.debug("[REGISTER OUT]");
            LOGGER.warn(checker.problem());
            return Result.fail();
        }
    }

    @RequestMapping(value = "/unregister", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Result> unregister(@RequestBody AlertSubscriber subscriber) {
        LOGGER.debug("[UNREGISTER IN]");
        AlertSubscriber recordedSubscriber = alertSubscriberRepository.findByGcmKey(subscriber.getGcmKey());
        LOGGER.debug(String.format("Secure token -> %s", subscriber.getSecureToken()));

        Checker checker = new Checker(CLIENT_ID, AUDIENCE_ID);
        Payload payload = checker.check(subscriber.getSecureToken());
        if (payload != null) {

            if (recordedSubscriber != null) {
                alertSubscriberRepository.delete(recordedSubscriber);
            }
            LOGGER.debug("[UNREGISTER OUT]");
            return Result.success();

        }
        LOGGER.debug("[UNREGISTER OUT]");
        LOGGER.warn(checker.problem());
        return Result.fail();
    }

}
