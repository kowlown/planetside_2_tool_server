package com.mystnihon.planetside.controllers.ui;

import com.mystnihon.planetside.database.entities.AlertSubscriber;
import com.mystnihon.planetside.database.repositories.AlertSubscriberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Default template
 * Created by Lévi LIRVAT on 09/05/2016.
 */
@RequestMapping("/admin")
@RestController
public class RegistrationViewController {

    @Autowired
    private AlertSubscriberRepository alertSubscriberRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView listPage(@RequestParam("page") int page, @RequestParam("size") int size) {

        final Page<AlertSubscriber> subscriberList = alertSubscriberRepository.findAll(new PageRequest(page, size));

        ModelAndView model = new ModelAndView("list");
        model.addObject("subscriberPage", subscriberList);
        if (subscriberList.hasPrevious()) {
            model.addObject("hasPreviousPage", subscriberList.hasPrevious());
            model.addObject("previousPageNumber", subscriberList.previousPageable().getPageNumber());
            model.addObject("previousPageSize", subscriberList.previousPageable().getPageSize());
        }
        if (subscriberList.hasNext()) {
            model.addObject("hasNextPage", subscriberList.hasNext());
            model.addObject("nextPageNumber", subscriberList.nextPageable().getPageNumber());
            model.addObject("nextPageSize", subscriberList.nextPageable().getPageSize());
        }
        model.addObject("currentPage", subscriberList.getNumber());
        return model;

    }
}
