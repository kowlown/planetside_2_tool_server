<%--suppress XmlPathReference --%>
<%--@elvariable id="watcherStatus" type="java.lang.String"--%>
<%--@elvariable id="numberRecord" type="java.lang.Integer"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="true" %>
<html>
<head>
    <sec:csrfMetaTags/>
    <script
            src="https://code.jquery.com/jquery-3.0.0.js"
            integrity="sha256-jrPLZ+8vDxt2FnE1zvZXCkCcebI/C8Dt5xyaQBjxQIo="
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"/>

    <link rel="stylesheet" href="<c:url value="/css/admin.css"/>"/>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <title>Home</title>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <span class="navbar-brand">Administration</span>
        <a href="<c:url value="/admin/logout.html" />" class="btn btn-default navbar-btn navbar-right"> Logout</a>
    </div>
</nav>
<div class="container">
    <h1>Welcome !</h1>
    <div id="watcher_block">

        <button id="start">Start watcher</button>
        <button id="stop">Stop watcher</button>
        <button id="test">Do test</button>

        <div>
            <span>Status running : </span><span id="id-running">${watcherStatus}</span>
        </div>
        <div>
            <span>Number records : </span><span>${numberRecord}</span>
        </div>
        <div>
            <ul>
                <c:forEach items="${countAlertByDay}" var="object">
                    <li>${object[0]} -- ${object[1]}

                    </li>
                </c:forEach>
            </ul>
        </div>

        <%--<ul>--%>
        <%--<li><a href="<c:url value="/admin/home/start.html"/>">Start</a></li>--%>
        <%--<li><a href="<c:url value="/admin/home/stop.html"/>">Stop</a></li>--%>
        <%--</ul>--%>
    </div>
    <div class="info"></div>
</div>
<script>
    var url = "<c:url value="/admin/home/action.ao"/>";
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var headers = {};
    headers[csrfHeader] = csrfToken;
</script>
<script src="<c:url value="/js/admin.js"/> "></script>
</body>
</html>