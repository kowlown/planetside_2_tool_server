<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Lévi LIRVAT
  Date: 10/06/2016
  Time: 17:08
  To change this template use File | Settings | File Templates.
--%>
<%--@elvariable id="subscriberPage" type="org.springframework.data.domain.Page"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <sec:csrfMetaTags/>
    <title>Registration list</title>
</head>
<body>
<div>
    <table>

<%--suppress ELValidationInJSP --%>
        <c:forEach items="${subscriberPage.getContent()}" var="subscriber">
        <tr>
            <td>${subscriber.id}</td>
            <td>${subscriber.gcmKey}</td>
        </tr>
    </c:forEach>
    </table>
</div>

</body>
</html>
