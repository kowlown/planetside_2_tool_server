$("#start").click(function () {
    $.ajax({
        method: "get",
        url: url,
        contentType: "application/json;charset=utf-8",
        success: jsonResponse,
        headers: headers,
        data: "action=start"
    });

});
$("#stop").click(function () {
    $.ajax({
        method: "get",
        url: url,
        contentType: "application/json;charset=utf-8",
        success: jsonResponse,
        headers: headers,
        data: "action=stop"
    });

});
$("#test").click(function () {
    $.ajax({
        method: "get",
        url: url,
        contentType: "application/json;charset=utf-8",
        success: jsonResponse,
        headers: headers,
        data: "action=test"
    });

});
function jsonResponse(json) {
    var obj = json;// $.parseJSON(json);
    if (obj == null) {
        obj = json;
    }
    if (obj != null) {
        if (obj.success == true) {
            displayInfo("Op&eacute;ration r&eacute;ussie");
        } else {
            displayInfo("Requ&ecirc;te en erreur");
        }
        $("#id-running").text(obj.status);
        window.setTimeout(function () {
            window.location.reload();
        }, 2000);
    }
}
function displayInfo(message) {
    $(".info").html(message).show("blind", function () {
        setTimeout(function () {
            $(".info:visible").removeAttr("style").fadeOut();
        }, 1000);
    });
}