package com.mystnihon.planetside;

import com.mystnihon.planetside.database.entities.*;
import com.mystnihon.planetside.database.repositories.MetagameEventRepository;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

import static com.ninja_squad.dbsetup.Operations.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Default template
 * Created by llirvat on 09/12/2016.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class DatabaseRepositoryTest {

    @Autowired
    MetagameEventRepository metagameEventRepository;

    @Autowired
    DataSource dataSource;

    @Before
    public void cleanUp() {
        Operation DELETE_ALL = deleteAllFrom(Description.TABLE_NAME, Name.TABLE_NAME, MetagameEvent.TABLE_NAME, World.TABLE_NAME, AlertSubscription.TABLE_NAME, AlertSubscriber.TABLE_NAME);

        Operation operation = sequenceOf(
                DELETE_ALL,
                insertInto(MetagameEvent.TABLE_NAME).columns(MetagameEvent.COLUMN_NAME_ID, MetagameEvent.COLUMN_NAME_METAGAME_EVENT_ID, MetagameEvent.COLUMN_NAME_TYPE, MetagameEvent.COLUMN_NAME_EXPERIENCE_BONUS)
                        .values(101L, 1, "1", "30")
                        .values(102L, 2, "1", "40")
                        .values(103L, 3, "1", "50")
                        .build(),
                insertInto(Name.TABLE_NAME).columns("id", Name.COLUMN_NAME_FR, Name.COLUMN_NAME_METAGAME_EVENT_ID)
                        .values(2, "toto1", 101L)
                        .values(3, "toto2", 102L)
                        .values(4, "toto3", 103L)
                        .build(),
                insertInto(Description.TABLE_NAME).columns("id", Description.COLUMN_NAME_FR, Description.COLUMN_NAME_METAGAME_EVENT_DESC_ID)
                        .values(1, "totoDesc1", 101L)
                        .values(2, "totoDesc2", 102L)
                        .values(3, "totoDesc3", 103L)
                        .build());

        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), operation);
        dbSetup.launch();

    }


    @Test
    public void metagameEventTest() {
        Optional<MetagameEvent> m = metagameEventRepository.findByMetagameEventId(1);
        assertThat(m).isPresent();
        assertThat(m.get()).extracting("id", "metagameEventId", "type", "experienceBonus")
                .containsExactlyInAnyOrder(101L, 1, "1", "30");
        List<MetagameEvent> list = metagameEventRepository.findAll();
        assertThat(list).hasSize(3);

        assertThat(metagameEventRepository.findAll()).extracting("name", Name.class).extracting("id").containsExactlyInAnyOrder(2L, 3L, 4L);
        assertThat(metagameEventRepository.findAll()).extracting("name", Name.class).extracting("fr").containsExactlyInAnyOrder("toto1", "toto2", "toto3");

        assertThat(metagameEventRepository.findAll()).extracting("description", Description.class).extracting("id").containsExactlyInAnyOrder(1L, 2L, 3L);
        assertThat(metagameEventRepository.findAll()).extracting("description", Description.class).extracting("fr").containsExactlyInAnyOrder("totoDesc1", "totoDesc2", "totoDesc3");
    }



}
