package com.mystnihon.planetside.configuration;

import com.mystnihon.planetside.database.repositories.AlertSubscriberRepository;
import com.mystnihon.planetside.database.repositories.AlertSubscriptionRepository;
import com.mystnihon.planetside.database.repositories.MetagameEventRepository;
import com.mystnihon.planetside.database.repositories.WorldRepository;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * Default template
 * Created by llirvat on 09/12/2016.
 */
@Configuration
public class RepositoryMockconfiguration {

    @Bean
    @Primary
    public AlertSubscriberRepository alertSubscriberRepositoryMock() {
        return Mockito.mock(AlertSubscriberRepository.class);
    }

    @Bean
    @Primary
    public AlertSubscriptionRepository alertSubscriptionRepositoryMock() {
        return Mockito.mock(AlertSubscriptionRepository.class);
    }

    @Bean
    @Primary
    public MetagameEventRepository metagameEventRepositoryMock() {
        return Mockito.mock(MetagameEventRepository.class);
    }

    @Bean
    @Primary
    public WorldRepository worldRepositoryMock() {
        return Mockito.mock(WorldRepository.class);
    }

}
