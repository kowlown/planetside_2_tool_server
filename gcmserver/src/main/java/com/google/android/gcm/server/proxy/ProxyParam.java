package com.google.android.gcm.server.proxy;

import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;

/**
 * 
 * @author L�vi LIRVAT
 * 
 */
public class ProxyParam {

	private Proxy proxy;
	private Authenticator authenticator;
	public String host, username, password;
	public int port;

	private ProxyParam() {
		proxy = Proxy.NO_PROXY;
	}

	/**
	 * Default constructor. Create the proxy with the host and port parameters.
	 * 
	 * @param host
	 *            host of the proxy
	 * @param port
	 *            port of the proxy
	 */
	public ProxyParam(String host, int port) {
		this(host, port, null, null);
	}

	/**
	 * Constructor with authentication.
	 * 
	 * @param host
	 *            host of the proxy
	 * @param port
	 *            port of the proxy
	 * @param username
	 *            username/login for the proxy if it requests authentication.
	 * @param password
	 *            password for the proxy if it requests authentication.
	 */
	public ProxyParam(String host, int port, final String username,
			final String password) {
		this();

		this.proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host,
				port));
		if (username != null && password != null) {
			this.authenticator = new Authenticator() {

				public PasswordAuthentication getPasswordAuthentication() {
					return (new PasswordAuthentication(username,
							password.toCharArray()));
				}
			};
		}
	}

	/**
	 * getter for the created proxy
	 * @return the Proxy
	 */
	public Proxy getProxy() {
		return proxy;
	}

	/**
	 * getter for the created authenticator
	 * @return the Authenticator
	 */
	public Authenticator getAuthenticator() {
		return authenticator;
	}
}
