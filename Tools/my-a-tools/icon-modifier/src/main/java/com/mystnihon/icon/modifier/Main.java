package com.mystnihon.icon.modifier;

import java.io.IOException;

/**
 * Default template
 * Created by llirvat on 28/02/2017.
 */
public class Main {
    public static void main(String[] args) {
        String path = args[0];
        String flavor = args[1].trim();
        try {
            new IconModifier().modifyAndSaveImage(path, flavor);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
