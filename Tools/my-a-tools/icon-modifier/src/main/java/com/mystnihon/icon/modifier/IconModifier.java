package com.mystnihon.icon.modifier;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Default template
 * Created by llirvat on 28/02/2017.
 */
public class IconModifier {


    public void modifyAndSaveImage(String imagePath, String flavorName) throws IOException {
        File imageFile = new File(imagePath);
        String str = imageFile.getName();
        BufferedImage bufferedImage = ImageIO.read(new FileInputStream(imageFile));
        BufferedImage finalImage = addFlavorEtiquette(bufferedImage, flavorName, 0.3f, 0.2f);
        ImageIO.write(finalImage, "png", new File(imageFile.getAbsoluteFile().getParentFile(), str.substring(0, str.lastIndexOf('.')) + "_" + flavorName + ".png"));
    }

    public BufferedImage addFlavorEtiquette(BufferedImage bufferedImage, String flavorName, float percentWidth, float percentHeight) {

        if (bufferedImage != null) {
            final int imageWidth = bufferedImage.getWidth();
            final int imageHeight = bufferedImage.getHeight();

            final int etiWidth = Math.round(Math.min(imageWidth, imageWidth * percentWidth));
            final int etiHeight = Math.round(Math.min(imageHeight, imageHeight * percentHeight));
            final int startX = imageWidth - etiWidth;
            final int startY = imageHeight - etiHeight;
            final int margin = 10;

            Graphics2D g = bufferedImage.createGraphics();

            Font font = g.getFont();

            FontMetrics metrics = g.getFontMetrics(font);
            // get the height of a line of text in this
            // font and render context
            int hgt = metrics.getHeight();
            // get the advance of my text in this font
            // and render context
            int adv = metrics.stringWidth(flavorName);
            // calculate the size of a box to hold the
            // text with some padding.
            Dimension size = new Dimension(adv + 2, hgt + 2);

            RoundRectangle2D roundRect = new RoundRectangle2D.Float(startX - margin, startY - margin, etiWidth, etiHeight, 3, 3);

            g.setPaint(Color.decode("#F9F9F9"));
            g.fill(roundRect);
            g.setPaint(Color.BLACK);
            g.drawString(flavorName, Math.round(roundRect.getCenterX() - size.getWidth() / 2), Math.round(roundRect.getCenterY()));
        }
        return bufferedImage;

    }
}
