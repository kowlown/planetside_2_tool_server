package com.mystnihon.bcrypt.generator;

import java.io.File;
import java.io.FileFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Default template
 * Created by llirvat on 22/02/2017.
 */
public class HashGeneratorFactory {

    public void discovery() throws MalformedURLException {

        FileFilter fileFilter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".jar");
            }
        };
        File localFile = new File(".");
        File[] files = localFile.listFiles(fileFilter);
        for (File file : files) {
            URL[] urls = new URL[]{file.toURI().toURL()};
            ClassLoader loader = new URLClassLoader(urls);
        }

    }
}
