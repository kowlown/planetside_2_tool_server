package com.mystnihon.bcrypt.generator;

/**
 * Default template
 * Created by llirvat on 22/02/2017.
 */
public interface HashGenerator {

    String encode(CharSequence rawPassword);
}
