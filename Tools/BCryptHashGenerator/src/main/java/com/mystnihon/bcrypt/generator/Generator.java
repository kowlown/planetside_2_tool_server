package com.mystnihon.bcrypt.generator;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.nio.CharBuffer;

/**
 * Default template
 * Created by Levi on 21/12/2016.
 */
public class Generator {
    public static final int DEFAULT_STRENGTH = 10;
    private IGeneratorListener mIGeneratorListener;
    private int mStrength = DEFAULT_STRENGTH;

    public void generateHash(char[] password) {
        if (password.length > 0) {
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(mStrength);
            String hash = bCryptPasswordEncoder.encode(CharBuffer.wrap(password));
            publish(hash);
        }
    }


    private void publish(String hash) {
        if (mIGeneratorListener != null) {
            mIGeneratorListener.onHashGenerated(hash);
        }
    }

    public void setStrength(int strength) {
        if (strength >= 10) {
            mStrength = strength;
        }
    }

    public void setGeneratorListener(IGeneratorListener generatorListener) {
        mIGeneratorListener = generatorListener;
    }

    public interface IGeneratorListener {
        void onHashGenerated(String hash);
    }

}
