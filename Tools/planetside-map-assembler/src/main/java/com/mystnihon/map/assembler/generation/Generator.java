package com.mystnihon.map.assembler.generation;

import com.mystnihon.map.assembler.generation.bean.RowBean;

import javax.imageio.ImageIO;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Default template
 * Created by Levi on 19/02/2017.
 */
public class Generator implements Runnable {

    public static final String FORMAT_NAME = "png";
    private File sourceFile;
    private File targetFile;
    private StatusListener listener;
    private int steps;
    private int step=0;

    public Generator(StatusListener listener) {
        this.listener = listener;
    }

    public void setSourceFile(File sourceFile) {
        this.sourceFile = sourceFile;
    }

    public void setTargetFile(File targetFile) {
        this.targetFile = targetFile;
    }

    @Override
    public void run() {
        TileInfoParser tileInfoParser = new TileInfoParser();
        List<String> missingFiles = new ArrayList<>();
        try {
            List<RowBean> rowBeen = tileInfoParser.parse(sourceFile);
            final int steps = rowBeen.size() + 1;
            setSteps(steps);
            Collections.sort(rowBeen, (o1, o2) -> {
                if (o1.sum() < o2.sum()) {
                    return -1;
                } else if (o1.sum() > o2.sum()) {
                    return 1;
                } else {
                    return 0;
                }
            });

            if (rowBeen.size() > 0) {
                RowBean minRow = rowBeen.get(0);
                int offsetX = Math.round(Math.abs(minRow.getPositionX()));
                int offsetY = Math.round(Math.abs(minRow.getPositionY()));
                double line = Math.sqrt(rowBeen.size());
                int width = (int) (minRow.getWidth() * line);
                int height = (int) (minRow.getHeight() * line);
                BufferedImage finalImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
                for (RowBean rowBean : rowBeen) {
                    File partFile = new File(sourceFile.getParent(), rowBean.getFilename());
                    if (partFile.exists()) {
                        BufferedImage image = ImageIO.read(partFile);


                        finalImage.createGraphics().drawImage(image, offsetX + Math.round(rowBean.getPositionX()), offsetY + Math.round(rowBean.getPositionY()), Math.round(rowBean.getWidth()), Math.round(rowBean.getHeight()), null);
                    } else {
                        missingFiles.add("Missing: " + partFile.getName());
                    }
                    publishProgress();
                }
                AffineTransform tx = AffineTransform.getScaleInstance(1, -1);
                tx.translate(0, -finalImage.getHeight(null));
                AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
                finalImage = op.filter(finalImage, null);
                ImageIO.write(finalImage, FORMAT_NAME, targetFile);
                publishProgress();
            }

            if (listener != null) {
                listener.onComplete();
            }
        } catch (IOException e) {
            if (listener != null) {
                listener.onError(e);
            }
        }
        for (String missingFile : missingFiles) {
            System.out.println(missingFile);
        }

    }

    private void publishProgress() {
        if (listener != null) {
            listener.onPublish(++step, steps);
        }
    }

    public void setSteps(int steps) {
        this.step = 0;
        this.steps = steps;
    }

    public interface StatusListener {
        void onComplete();

        void onPublish(int stet, int total);

        void onError(Exception e);
    }
}
