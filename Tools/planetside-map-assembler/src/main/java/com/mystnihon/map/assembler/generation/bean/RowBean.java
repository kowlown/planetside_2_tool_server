package com.mystnihon.map.assembler.generation.bean;

import com.opencsv.bean.CsvBindByPosition;

/**
 * Created by Levi on 18/02/2017.
 */
public class RowBean {
    @CsvBindByPosition(position = 0)
    private String filename;

    @CsvBindByPosition(position = 3)
    private float positionX;
    @CsvBindByPosition(position = 1)
    private float positionY;
    @CsvBindByPosition(position = 2)
    private float positionZ;

    @CsvBindByPosition(position = 4)
    private float width;
    @CsvBindByPosition(position = 5)
    private float height;

    @CsvBindByPosition(position = 6)
    private float var1;
    @CsvBindByPosition(position = 7)
    private float var2;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public float getPositionZ() {
        return positionZ;
    }

    public void setPositionZ(float positionZ) {
        this.positionZ = positionZ;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getVar1() {
        return var1;
    }

    public void setVar1(float var1) {
        this.var1 = var1;
    }

    public float getVar2() {
        return var2;
    }

    public void setVar2(float var2) {
        this.var2 = var2;
    }

    public float sum() {
        return positionX + positionY + positionZ;
    }
}
