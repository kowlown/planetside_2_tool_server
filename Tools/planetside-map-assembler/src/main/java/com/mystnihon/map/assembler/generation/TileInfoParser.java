package com.mystnihon.map.assembler.generation;

import com.mystnihon.map.assembler.generation.bean.RowBean;
import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

/**
 * Default template
 * Created by Levi on 18/02/2017.
 */
public class TileInfoParser {

    public List<RowBean> parse(File file) throws FileNotFoundException {
        ColumnPositionMappingStrategy<RowBean> rowBeanColumnPositionMappingStrategy = new ColumnPositionMappingStrategy<>();
        rowBeanColumnPositionMappingStrategy.setType(RowBean.class);
        CsvToBean<RowBean> csvToBean = new CsvToBean<>();
        return csvToBean.parse(rowBeanColumnPositionMappingStrategy, createReader(file));
    }

    private CSVReader createReader(File file) throws FileNotFoundException {
        return new CSVReader(new FileReader(file), '\t');
    }

}
