package com.mystnihon.map.assembler;

import com.mystnihon.map.assembler.gui.MapAssemblerForm;

/**
 * Default template
 * Created by Levi on 18/02/2017.
 */
public class Main {
    public static void main(String[] args) {
        new MapAssemblerForm();
    }
}
