package com.mystnihon.map.assembler.gui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import com.mystnihon.map.assembler.generation.Generator;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Default template
 * Created by Levi on 18/02/2017.
 */
public class MapAssemblerForm extends JFrame implements ActionListener, Generator.StatusListener, KeyListener {
    private static final String DEFAULT_PATH = "default.path";
    private static final String PREFS_FILE_NAME = "prefs";
    private static final String COMMENTS = "";
    private static final String TARGET_PATH = "target.path";
    private JTextField mTextFieldSource;
    private JTextField mTextFieldTarget;
    private JButton mSourceButton;
    private JButton mTargetButton;
    private JPanel panel;
    private JButton mGenerateButton;
    private JProgressBar mProgressBar1;

    private Generator mGenerator;
    private Thread mThread;

    public MapAssemblerForm() {
        add(panel);
        pack();
        addListeners();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        mGenerator = new Generator(this);
    }

    private void addListeners() {
        mSourceButton.addActionListener(this);
        mTargetButton.addActionListener(this);
        mGenerateButton.addActionListener(this);
        mTextFieldTarget.addKeyListener(this);
        mTextFieldSource.addKeyListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == mSourceButton) {
            String lastSourcePath = loadProperty(DEFAULT_PATH, ".");
            File sourceFile = Optional.ofNullable(getFile(ResourceBundle.getBundle("lang").getString("filechooser.source.title"), JFileChooser.FILES_ONLY, lastSourcePath, false, ".txt")).orElse(new File(""));
            if (sourceFile != null && sourceFile.getParentFile() != null) {
                saveProperty(DEFAULT_PATH, sourceFile.getParentFile().getAbsolutePath());
                mTextFieldSource.setText(sourceFile.getAbsolutePath());
            }
        } else if (e.getSource() == mTargetButton) {
            String lastTargetPath = loadProperty(TARGET_PATH, ".");
            File targetFile = Optional.ofNullable(getFile(ResourceBundle.getBundle("lang").getString("filechooser.target.title"), JFileChooser.FILES_ONLY, lastTargetPath, true)).orElse(new File(""));
            if (targetFile != null && targetFile.getParentFile() != null) {
                saveProperty(TARGET_PATH, targetFile.getParentFile().getAbsolutePath());
                mTextFieldTarget.setText(targetFile.getAbsolutePath());
            }
        } else if (e.getSource() == mGenerateButton) {
            mGenerateButton.setEnabled(false);
            mProgressBar1.setVisible(true);
            final String source = mTextFieldSource.getText();
            final String target = mTextFieldTarget.getText();

            if (source.isEmpty() || target.isEmpty()) {
                System.out.println("");
            } else {

                mGenerator.setSourceFile(new File(source));
                mGenerator.setTargetFile(new File(target));
                if (mThread != null) {
                    mThread.interrupt();
                }
                mThread = new Thread(mGenerator);
                mThread.start();
            }
        }
    }

    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent e) {

    }

    private String loadProperty(String key, String defaultValue) {
        try {
            Properties property = new Properties();
            property.load(new FileInputStream(new File(PREFS_FILE_NAME)));
            return property.getProperty(key, defaultValue);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    private void saveProperty(String key, String value) {

        try {
            Properties property = new Properties();
            property.load(new FileInputStream(new File(PREFS_FILE_NAME)));
            property.setProperty(key, value);
            property.store(new FileOutputStream(new File(PREFS_FILE_NAME)), COMMENTS);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getFile(String chooserTitle, int mode, String path, boolean save, String... filter) {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File(path));
        chooser.setDialogTitle(chooserTitle);
        chooser.setFileSelectionMode(mode);
        if (filter != null && filter.length > 0) {
            chooser.addChoosableFileFilter(new Filter(filter));
        }
        chooser.setAcceptAllFileFilterUsed(false);
        if (save && chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile();
        } else if (!save && chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            switch (mode) {
                case JFileChooser.DIRECTORIES_ONLY:
                    return chooser.getCurrentDirectory();
                default:
                    return chooser.getSelectedFile();
            }

        } else {
            return null;
        }
    }

    @Override
    public void onComplete() {
        System.out.println("Complete");
        mProgressBar1.setVisible(false);
        mGenerateButton.setEnabled(true);
    }

    @Override
    public void onPublish(int step, int total) {
        System.out.println("Step:" + step + " Total:" + total);
        mProgressBar1.setMaximum(total);
        mProgressBar1.setValue(step);

    }

    @Override
    public void onError(Exception e) {
        System.err.println("Error:" + e);
        mGenerateButton.setEnabled(true);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel = new JPanel();
        panel.setLayout(new GridLayoutManager(5, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel.setAutoscrolls(false);
        panel.setVisible(true);
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        mTextFieldSource = new JTextField();
        panel1.add(mTextFieldSource, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        mSourceButton = new JButton();
        this.$$$loadButtonText$$$(mSourceButton, ResourceBundle.getBundle("lang").getString("button.select.source.dir"));
        panel1.add(mSourceButton, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel.add(panel2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        mTextFieldTarget = new JTextField();
        panel2.add(mTextFieldTarget, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        mTargetButton = new JButton();
        this.$$$loadButtonText$$$(mTargetButton, ResourceBundle.getBundle("lang").getString("button.select.target.dir"));
        panel2.add(mTargetButton, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        mGenerateButton = new JButton();
        this.$$$loadButtonText$$$(mGenerateButton, ResourceBundle.getBundle("lang").getString("button.generate.bitmap"));
        panel.add(mGenerateButton, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        mProgressBar1 = new JProgressBar();
        panel.add(mProgressBar1, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel.add(spacer1, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private void $$$loadButtonText$$$(AbstractButton component, String text) {
        StringBuffer result = new StringBuffer();
        boolean haveMnemonic = false;
        char mnemonic = '\0';
        int mnemonicIndex = -1;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == '&') {
                i++;
                if (i == text.length()) break;
                if (!haveMnemonic && text.charAt(i) != '&') {
                    haveMnemonic = true;
                    mnemonic = text.charAt(i);
                    mnemonicIndex = result.length();
                }
            }
            result.append(text.charAt(i));
        }
        component.setText(result.toString());
        if (haveMnemonic) {
            component.setMnemonic(mnemonic);
            component.setDisplayedMnemonicIndex(mnemonicIndex);
        }
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel;
    }

    class Filter extends FileFilter {

        String[] filter;

        public Filter(String[] filter) {
            this.filter = filter;
        }

        @Override
        public boolean accept(File f) {
            if (f.isDirectory()) {
                return true;
            }
            for (String s : filter) {
                if (f.getName().endsWith(s)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public String getDescription() {
            return "Standard filter";
        }
    }


}
